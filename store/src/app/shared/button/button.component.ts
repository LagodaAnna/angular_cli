import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styles: [
    'button { width: 120px; height: 50px; border:none; border-radius:10px; background-color:#FFDAB9; transition: background-color 250ms; cursor:pointer}',
    'button:hover{background-color:#FFE4E1;}'
  ]
})
export class ButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

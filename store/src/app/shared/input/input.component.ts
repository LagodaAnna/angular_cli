import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styles: [
     'input { margin-right: 20px; width: 180px; height: 44px; border: 1px solid grey; border-radius:10px; }',
  ]
})
export class InputComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

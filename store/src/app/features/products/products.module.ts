import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'src/app/shared/button/button.module';
import { InputModule } from 'src/app/shared/input/input.module';
import { ProductsComponent } from './products.component';
import { ProductModule } from './product/product.module';



@NgModule({
  declarations: [
    ProductsComponent
  ],
  imports: [
    CommonModule, ButtonModule, InputModule, ProductModule
  ],
  exports: [
     ProductsComponent
  ]

})
export class ProductsModule { }
